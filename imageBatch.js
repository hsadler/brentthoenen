const fs = require('fs')
const path = require('path')

const buildObject = name => ({ title: createTitle(name), url: `/images/${name}` })

const createTitle = name => createSpaces(removeExtension(name))
const createSpaces = name => name.replace(/([A-Z])/g, ' $1').trim()
const removeExtension = name => name.replace(/\.[^.]*$/g, '')

fs.readdir(path.resolve(__dirname, 'src', 'images'), (err, files) => {
    const objs = files.map(file => buildObject(file))
    fs.writeFile(path.resolve(__dirname, 'src', 'images.json'), JSON.stringify(objs, null, 2), 'utf8', () => console.log('Batch Complete!'))
})