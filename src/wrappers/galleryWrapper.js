import { connect } from 'react-redux'
import Gallery from '../components/gallery/gallery'
import { nextImage } from '../actions'
import { currentImageSelector, upcomingImagesSelector } from '../selectors'

const mapStateToProps = state => ({ 
    currentImage: currentImageSelector(state),
    upcomingImages: upcomingImagesSelector(state),
})

const mapDispatchToProps = dispatch => ({
    nextImage: () => dispatch(nextImage())
})

const GalleryWrapper = connect(mapStateToProps, mapDispatchToProps)(Gallery)

export default GalleryWrapper