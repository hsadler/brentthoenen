import { connect } from 'react-redux'
import App from '../components/app/app'
import { introDisplayedSelector } from '../selectors'

const mapStateToProps = state => ({ 
    introDisplayed: introDisplayedSelector(state),
})

const mapDispatchToProps = dispatch => ({ })

const AppWrapper = connect(mapStateToProps, mapDispatchToProps)(App)

export default AppWrapper