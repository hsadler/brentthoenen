import { connect } from 'react-redux'
import Intro from '../components/intro/intro'
import { introOver } from '../actions'

const mapStateToProps = state => ({ })

const mapDispatchToProps = dispatch => ({ 
    introOver: () => dispatch(introOver()),
})

const IntroWrapper = connect(mapStateToProps, mapDispatchToProps)(Intro)

export default IntroWrapper