import { combineReducers } from 'redux'
import shuffle from 'lodash.shuffle'
import { INTRO_OVER, NEXT_IMAGE } from '../actions'
import Images from '../images.json'

const images = (state = [], action) => {
    switch(action.type) {
        case INTRO_OVER:
            return shuffle(Images)
        case NEXT_IMAGE:
            return state.length > 1 ? state.slice(1) : shuffle(Images)
        default:
            return state
    }
}

const introDisplayed = (state = false, action) =>  {
    switch(action.type) {
        case INTRO_OVER:
            return true
        default:
            return state
    }
}

const reducer = combineReducers({
    images,
    introDisplayed
})

export default reducer