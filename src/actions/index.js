const INTRO_OVER = "INTRO_OVER"
const introOver = () => ({ type: INTRO_OVER })

const NEXT_IMAGE = "NEXT_IMAGE"
const nextImage = () => ({ type: NEXT_IMAGE })

export {
    INTRO_OVER,
    introOver,
    NEXT_IMAGE,
    nextImage,
}