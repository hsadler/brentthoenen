import { createSelector } from 'reselect'

const imagesSelector = state => state.images
const introDisplayedSelector = state => state.introDisplayed

const currentImageSelector = createSelector(
    imagesSelector,
    images => images[0]
)

const upcomingImagesSelector = createSelector(
    imagesSelector,
    images => images.slice(0, 3)
)

export {
    imagesSelector,
    introDisplayedSelector,
    currentImageSelector,
    upcomingImagesSelector,
}