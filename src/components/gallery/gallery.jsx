import React from 'react'
import { Image } from 'semantic-ui-react'

class Gallery extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="gallery">
                <div className="imageContainer" onClick={this.props.nextImage}>
                    {/* <div className="image" style={{backgroundImage: `url(${this.props.currentImage.url})`}}></div> */}
                    <Image src={this.props.currentImage.url}/>
                </div>
                <div className="title">{this.props.currentImage.title}</div>
                { this.props.upcomingImages.map(image => <Image key={image.url} className="hidden" src={image.url}/>) }
            </div>
        )
    }
}

export default Gallery