import React from 'react'
import GalleryWrapper from '../../wrappers/galleryWrapper'
import IntroWrapper from '../../wrappers/introWrapper'

class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return this.props.introDisplayed ? <GalleryWrapper/> : <IntroWrapper/> 
    }
}

export default App