import React from 'react'

class Sentence extends React.Component {
    constructor(props) {
        super(props)

        this.nextAnimationCB = null

        this.state = {
            currentAnimation: this.props.startAnimation,
            duration: this.props.startTime,
            section: "start"
        }
    }

    componentDidMount() {
        // Starts Wait
        this.nextAnimationCB = window.setTimeout(() => this.setState({ currentAnimation: "", duration: this.props.waitTime, section: "wait"}), this.props.startTime * 1000)
    }

    componentWillUnmount() {
        clearTimeout(this.nextAnimationCB)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ currentAnimation: nextProps.startAnimation, duration: this.props.startTime, section: "start" })
        // Starts Wait
        this.nextAnimationCB = window.setTimeout(() => this.setState({ currentAnimation: "", duration: this.props.waitTime, section: "wait"}), nextProps.startTime * 1000)
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.section === "wait") {
            // Starts End
            this.nextAnimationCB = window.setTimeout(() => this.setState({ currentAnimation: this.props.endAnimation, duration: this.props.endTime, section: "end" }), this.props.waitTime * 1000)
        } else if (this.state.section === "end") {
            // Marks End
            this.nextAnimationCB = window.setTimeout(() => this.setState({ currentAnimation: "", duration: 0, section: "" }), this.props.endTime * 1000)
        } else if (this.state.section === "") {
            // Animation Over
            this.props.endCallback()
        }
    }

    render() {
        return(
            <div 
                className={`sentence animated ${this.state.currentAnimation} ${this.props.className}`}
                style={{animationDuration: `${this.state.duration}s`}}
            >
                    {this.state.section !== "" ?  this.props.content : ""}
            </div>
        )
    }
}

export default Sentence