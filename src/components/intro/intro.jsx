import React from 'react'
import Sentence from '../sentence/sentence'

class Intro extends React.Component {
    constructor(props) {
        super(props)

        this.sentences = [
            { id: 1, content: "They said the dinosaurs were gone...", startAnimation: "fadeIn", endAnimation: "fadeOut", startTime: 3, endTime: 2, waitTime: 1},
            { id: 2, content: "It turns out, there's one left...", startAnimation: "fadeIn", endAnimation: "fadeOut", startTime: 3, endTime: 2, waitTime: 1},
            { id: 3, content: "This dinosaur isn't what you expect", startAnimation: "fadeIn", endAnimation: "fadeOut", startTime: 3, endTime: 2, waitTime: 1},
            { id: 4, content: "It's One", startAnimation: "fadeIn", endAnimation: "fadeOut", startTime: 2, endTime: 1, waitTime: 0.5},
            { id: 5, content: "BIG", startAnimation: "bounceIn", endAnimation: "bounceOut", startTime: 0.5, endTime: 0.5, waitTime: 0.5},
            { id: 6, content: "DUMB", startAnimation: "bounceIn", endAnimation: "bounceOut", startTime: 0.5, endTime: 0.5, waitTime: 0.5},
            { id: 7, content: "IDIOT", startAnimation: "zoomIn", endAnimation: "zoomOut", startTime: 1, endTime: 2, waitTime: 0.5},
            { id: 8, content: "Tap the image to explore the many phases of the Brontosaurus", startAnimation: "fadeIn", endAnimation: "fadeOut", startTime: 1, endTime: 1, waitTime: 2},
        ]

        this.state = { currentSentence: 1, taps: 0 }
        this.handleTap = this.handleTap.bind(this)
        this.nextSentence = this.nextSentence.bind(this)
    }

    handleTap() {
        this.setState((prevState, props) => ({ taps: prevState.taps + 1 }))
    }

    nextSentence() {
        const nextSentence = this.sentences.find(sentence => sentence.id === this.state.currentSentence + 1)
        if (nextSentence !== undefined) {
            this.setState({ currentSentence: nextSentence.id })
        } else {
            this.props.introOver()
        }
    }

    componentDidUpdate() {
        if (this.state.taps >= 5) {
            this.props.introOver()
        }
    }

    render() {
        const currentSentence = this.sentences.find(sentence => sentence.id === this.state.currentSentence)

        return (
            <div className="intro" onClick={this.handleTap}>
                <Sentence {...currentSentence} className={`sentence${currentSentence.id}`} endCallback={this.nextSentence}/>
            </div>
        )
    }
}

export default Intro